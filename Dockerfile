# Stage 1: Compile and Build angular codebase

# Use official node image as the base image
FROM  node:14.15.4 as build


USER docker
# Set the working directory
WORKDIR /usr/local/app

# Add the source code to app
COPY ./ /usr/local/app/



# Install all the dependencies
RUN npm install -g @angular/cli@13.2.3 


# Generate the build of the application
RUN ng serve -o 


expose 4200:4200