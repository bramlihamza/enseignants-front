import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnseignantsAjouterComponent } from './enseignants-ajouter/enseignants-ajouter.component';
import { EnseignantsroutingModule } from './enseignantsrouting/enseignantsrouting.module';
import { EnseignantsModifierComponent } from './enseignants-modifier/enseignants-modifier.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListEnseignantsComponent } from './list-enseignants/list-enseignants.component';
import { RouterModule, Routes } from '@angular/router';



@NgModule({
  declarations: [
    EnseignantsAjouterComponent,
    EnseignantsModifierComponent,
    ListEnseignantsComponent
  ],
  imports: [
    CommonModule,
    EnseignantsroutingModule,
    FormsModule,
    ReactiveFormsModule, 
  ]
})
export class EnseignantsModule { }
