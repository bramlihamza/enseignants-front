import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../../../Models/Utilisateur';
import { EnseignantService } from '../../../services/enseignant.service';

@Component({
  selector: 'app-list-enseignants',
  templateUrl: './list-enseignants.component.html',
  styleUrls: ['./list-enseignants.component.scss']
})
export class ListEnseignantsComponent implements OnInit {

  enseignants : Utilisateur[];
  modelpath="enseignants"
  constructor( private crud : EnseignantService) { }

  ngOnInit() {
    this.loadEnseignant();
  }

  loadEnseignant() {
    this
      .crud
      .findAll(this.modelpath)
      .subscribe(data => {
        this.enseignants = data;
      }, error => {
       
        console.log(error);
      });
  }

  deleteEnseignant(id) {

    this
      .crud.delete(this.modelpath,id)
      .subscribe(data => {
        console.log(data);
        this.loadEnseignant();
      }, error => {
        console.log(error);
      });
  }


}
