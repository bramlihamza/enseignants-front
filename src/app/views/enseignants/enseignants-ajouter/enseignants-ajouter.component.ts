import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Utilisateur } from '../../../Models/Utilisateur';
import { EnseignantService } from '../../../services/enseignant.service';


@Component({
  selector: 'app-enseignants-ajouter',
  templateUrl: './enseignants-ajouter.component.html',
  styleUrls: ['./enseignants-ajouter.component.scss']
})
export class EnseignantsAjouterComponent implements OnInit  {

  private type : any;

  
  public user:Utilisateur = {
    nom: '',
    email: '',
    id: 0,
    password: '',
    prenom: '',
    role: '',
    type: '',
    token: ''
  }; 

  ChooseCriteria = [
    { category: "Maitre Assistant", id: 1, isSelected: false },
    { category: "Vacataire", id: 2, isSelected: false },
    { category: "Formateur", id: 3, isSelected: false }
  ];
  isChecked;
  isCheckedName;
  gForm: FormGroup;
ID:number ;
  constructor( private formBuilder: FormBuilder ,
    
     private crud : EnseignantService ) {
    
   }

  ngOnInit(): void {
    this.gForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      type: ['', Validators.required]
  });
  }

  // get f() { return this.gForm.controls; }
  // getnom() { return this.gForm.get('nom').value; }
  // getprenom() { return this.gForm.get('prenom').value; }
  // getemail() { return this.gForm.get('email').value; }
  // getpassword() { return this.gForm.get('password').value; }
  // gettype() { return this.gForm.get('type').value; }

  

  onSubmit(data) {

    console.log("gform nom" +this.user.nom);
    let dataa = this.gForm.value ;
    
   this.ChooseCriteria.forEach(val => {
     if (val.isSelected == true) 
     dataa.type=val.category;
   });
   

this.crud.save(dataa).subscribe(
  (res : any) => {
    console.log(res);
  },
  (err) => {
    console.log(err);
  }
) ;
      }
  onChange(e){    
     
    this.ChooseCriteria.forEach(val => {
      if (val.id == e.id) val.isSelected = !val.isSelected;
      else {
        val.isSelected = false;
      }
    });
  }
  
}
