import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { EnseignantsAjouterComponent } from '../enseignants-ajouter/enseignants-ajouter.component';
import { EnseignantsModifierComponent } from '../enseignants-modifier/enseignants-modifier.component';
import { ListEnseignantsComponent } from '../list-enseignants/list-enseignants.component';


const routes: Routes = [
  {
    path: 'enseignants',
    data: {
      title: 'enseignants'
    },
    children: [
      {
        path: 'enseignantsAjouter',
        component: EnseignantsAjouterComponent,
        data: {
          title: 'enseignantsAjouter'
        }
      },
      {
        path: 'enseignantsModifier/:id',
        component: EnseignantsModifierComponent,
        data: {
          title: 'enseignantsModifier'
        }
      },
      {
        path: 'listenseignants',
        component: ListEnseignantsComponent,
        data: {
          title: 'listenseignants'
        }
      }
    ]
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnseignantsroutingModule { }
