import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CoursAjouterComponent } from '../cours-ajouter/cours-ajouter.component';


const routes: Routes = [
  {
    path: 'cours',
    data: {
      title: 'cours'
    },
    children: [
      {
        path: 'coursAjouter',
        component: CoursAjouterComponent,
        data: {
          title: 'coursAjouter'
        }
      },
      
    ]
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursRoutingModule { }
