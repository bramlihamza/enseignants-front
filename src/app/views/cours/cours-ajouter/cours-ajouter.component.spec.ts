import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursAjouterComponent } from './cours-ajouter.component';

describe('CoursAjouterComponent', () => {
  let component: CoursAjouterComponent;
  let fixture: ComponentFixture<CoursAjouterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoursAjouterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursAjouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
