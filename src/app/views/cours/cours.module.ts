import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursAjouterComponent } from './cours-ajouter/cours-ajouter.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoursRoutingModule } from './cours-routing/cours-routing.module';
import { ListCoursComponent } from './list-cours/list-cours.component';
import { CoursModifierComponent } from './cours-modifier/cours-modifier.component';



@NgModule({
  declarations: [
    CoursAjouterComponent,
    ListCoursComponent,
    CoursModifierComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule, 
    CoursRoutingModule
  ]
})
export class CoursModule { }
