import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursModifierComponent } from './cours-modifier.component';

describe('CoursModifierComponent', () => {
  let component: CoursModifierComponent;
  let fixture: ComponentFixture<CoursModifierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoursModifierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursModifierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
