import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartementsAjouterComponent } from './departements-ajouter.component';

describe('DepartementsAjouterComponent', () => {
  let component: DepartementsAjouterComponent;
  let fixture: ComponentFixture<DepartementsAjouterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartementsAjouterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartementsAjouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
