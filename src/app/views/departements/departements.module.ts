import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartementsAjouterComponent } from './departements-ajouter/departements-ajouter.component';
import { ListDepartementsComponent } from './list-departements/list-departements.component';
import { DepartementsModifierComponent } from './departements-modifier/departements-modifier.component';
import { DepartementsRoutingModule } from './departements-routing/departements-routing.module';



@NgModule({
  declarations: [
    DepartementsAjouterComponent,
    ListDepartementsComponent,
    DepartementsModifierComponent
  ],
  imports: [
    CommonModule,
    DepartementsRoutingModule,
  ]
})
export class DepartementsModule { }
