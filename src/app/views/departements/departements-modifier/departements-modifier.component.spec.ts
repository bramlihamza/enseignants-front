import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartementsModifierComponent } from './departements-modifier.component';

describe('DepartementsModifierComponent', () => {
  let component: DepartementsModifierComponent;
  let fixture: ComponentFixture<DepartementsModifierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartementsModifierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartementsModifierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
