import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DepartementsAjouterComponent } from '../departements-ajouter/departements-ajouter.component';
import { DepartementsModifierComponent } from '../departements-modifier/departements-modifier.component';
import { ListDepartementsComponent } from '../list-departements/list-departements.component';

  const routes: Routes = [
{
  path: 'departements',
  data: {
    title: 'departements'
  },
  children: [
    {
      path: 'departementsAjouter',
      component: DepartementsAjouterComponent,
      data: {
        title: 'departementsAjouter'
      }
    },
    {
      path: 'departementsModifier/:id',
      component: DepartementsModifierComponent,
      data: {
        title: 'departementsModifier'
      }
    },
    {
      path: 'listdepartements',
      component: ListDepartementsComponent,
      data: {
        title: 'listdepartements'
      }
    }
  ]
}];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
  })
  
export class DepartementsRoutingModule { }


