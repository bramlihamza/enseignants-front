import {Component} from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { navItems } from '../../_nav';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  public sidebarMinimized = false;
  public navItems = navItems;


  currentUser :any;
  helper: any;

  constructor(private authentication : AuthenticationService){
    
    let userFromStorage = localStorage.getItem('currentUtilisateur');
    let tokenObj = JSON.parse(userFromStorage);
     this.currentUser =  JSON.parse(atob(tokenObj.token.split('.')[1])).sub;
    
  }
  toggleMinimize(e) {
    this.sidebarMinimized = e;
   
  }


  logout() {
    this.authentication.logout();
  }

}
