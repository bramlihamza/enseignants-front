import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GenericCRUDService } from './generic-crud.service';

@Injectable({
  providedIn: 'root'
})
export class CoursService extends GenericCRUDService <any, any> {

  constructor(protected _http: HttpClient) { 
    super(_http,'http://localhost:8080/rest/cours');
  }
}
