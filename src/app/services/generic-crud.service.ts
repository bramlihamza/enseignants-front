import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CrudOperations } from './CrudOperations';

@Injectable({
  providedIn: 'root'
})
export class GenericCRUDService <T,ID extends CrudOperations<T, ID>> {
  
  constructor(
    protected _http: HttpClient,
    protected _base : string
  ) {
    
    console.log(this._base);
  }

  save(t: T): Observable<T> {
    return this._http.post<T>(this._base + "/insert" , t);
  }

  update(id: ID, t: T): Observable<T> {
    return this._http.patch<T>(this._base + "/update/"  + id, t, {});
  }

  findOne(id: ID): Observable<T> {
    return this._http.get<T>(this._base + "/"  + id);
  }

  findAll(modelpath): Observable<T[]> {
    return this._http.get<T[]>(this._base + "/all")
  }

  delete(modelpath,id: ID): Observable<T> {
    return this._http.delete<T>(this._base  + "/remove/" + id);
	}

}
